# limg
Take images and produce labels pointing to various parts of the image, producing a new SVG output for further processing.

This is designed to be used as part of an automated workflow that takes rendered models or similar things, and adds descriptive information to them for end user consumption. In particular, modifications to the input image(s) should be fairly well tolerated. In the long term, adding feature discovery and "zooming" multi-image capability (similar to the diagrams you often see where multiple scales of an object are labelled and diagrammed in one image) is desirable. 

For now, this library and the associated binary is just being designed for use in the generation of diagrams for the [computing.hardware.autonomy](https://gitlab.com/computing.hardware.autonomy) project. As such, it's a low priority for the author at the moment (and some parts of the code may seem rushed as the author really wants to get on to more interesting parts than making diagrams...) - but contributions are always welcome.

The project is licensed under GPLv3+
